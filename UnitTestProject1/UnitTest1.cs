﻿using ConsoleApp2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int num1;
            num1 = Program.func1(123);
            Assert.AreEqual(2, num1);
        }
        [TestMethod]
        public void TestMethod2()
        {
            int num1;
            num1 = Program.func2(123);
            Assert.AreEqual(3, num1);
        }
        [TestMethod]
        public void TestMethod3()
        {
            int num1;
            num1 = Program.func3(123);
            Assert.AreEqual(1, num1);
        }
    }
}

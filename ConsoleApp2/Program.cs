﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int a, x, y, z;

            Console.Write("Введите трёхзначное число: ");
            a = Convert.ToInt32(Console.ReadLine());

            while ((a > 999) | (a < 100))
            {
                Console.Write("Ошибка!Введите число от 100 до 999: ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
            }

            x = func1(a);
            y = func2(a);
            z = func3(a);
            Console.WriteLine($"Если прочитать число справа налево, получится: {y}{x}{z}");
            Console.ReadKey();
        }
        public static int func1(int a)
        {
            int x;
            x = (a / 10) % 10;
            return x;
        }

        public static int func2(int a)
        {
            int y;
            y = a % 10;
            return y;
        }

        public static int func3(int a)
        {
            int z;
            z = a / 100;
            return z;
        }
        
    }
}
